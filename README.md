# gm-api-sdk

用于GM后台API的SDK ...

[![NPM Version][npm-image]][npm-url]
[![NPM Downloads][downloads-image]][downloads-url]
[![Gratipay][licensed-image]][licensed-url]

[npm-image]: https://img.shields.io/npm/v/gm-api-sdk.svg
[npm-url]: https://www.npmjs.org/package/gm-api-sdk
[downloads-image]: https://img.shields.io/npm/dt/gm-api-sdk.svg
[downloads-url]: https://npmjs.org/package/gm-api-sdk
[licensed-image]: https://img.shields.io/badge/license-MIT-blue.svg
[licensed-url]: ttps://gitee.com/lianyujoy/gm-api-sdk/blob/master/LICENSE

## Install

```bash
yarn add gm-api-sdk
```

## Usages

```js
import { Protobuffer, Socket } from 'gm-api-sdk'

//定义配置
const options = {
  path     : './pb',
  socket   : 'socket.proto',
  gmPB     : 'gm.proto',
  prefix   : 'com.autorun'
}

const socket = new Socket('192.168.1.92:7015')
const protobuffer = new Protobuffer(options)
const { createPBBuffer, makeData, decode, gameMessage } = protobuffer

const data = makeData(createPBBuffer(2000, 'game.CS_GM_QUERY', { roleId: '000062' }))
const messageQuery = gameMessage('SC_GM_QUERY')

const sendPB = async (data) => {
  try {
    let buffer = await socket.send(data)
    let msgBase = decode(buffer, messageQuery)
    let { msghead, msgbody } = msgBase
    console.log(JSON.stringify(msghead, null, 2))
    console.log(JSON.stringify(msgbody, null, 2))
  } catch (error) {
    console.log('error', errer)
  }
}

sendPB(data)
```

## License

this repo is released under the [MIT License](https://gitee.com/lianyujoy/gm-api-sdk/blob/master/LICENSE).
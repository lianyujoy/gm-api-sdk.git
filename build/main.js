require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _protobuffer = __webpack_require__(1);

Object.defineProperty(exports, 'Protobuffer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_protobuffer).default;
  }
});

var _socket = __webpack_require__(7);

Object.defineProperty(exports, 'Socket', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_socket).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _protobufjs = __webpack_require__(2);

var _protobufjs2 = _interopRequireDefault(_protobufjs);

var _path = __webpack_require__(3);

var _path2 = _interopRequireDefault(_path);

var _fsExtra = __webpack_require__(4);

var _fsExtra2 = _interopRequireDefault(_fsExtra);

var _zlib = __webpack_require__(5);

var _zlib2 = _interopRequireDefault(_zlib);

var _pythonStruct = __webpack_require__(6);

var _pythonStruct2 = _interopRequireDefault(_pythonStruct);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var defaultOpts = {
  path: './pb',
  socket: 'socket.proto',
  gmPB: 'gm.proto',
  prefix: 'com.autorun'
};

var options = null;
var Root = null;

var protobuffer = function protobuffer(opts) {
  _classCallCheck(this, protobuffer);

  _initialiseProps.call(this);

  options = Object.assign({}, defaultOpts, opts);
  this.ProtoBuffer = getProtoBuffer(options);
  var _ProtoBuffer = this.ProtoBuffer,
      socketRoot = _ProtoBuffer.socketRoot,
      CMsgBase = _ProtoBuffer.CMsgBase,
      CMsgHead = _ProtoBuffer.CMsgHead,
      GMRoot = _ProtoBuffer.GMRoot;

  Root = new Map();
  Root.set('sockt', socketRoot);
  Root.set('game', GMRoot);
};

var _initialiseProps = function _initialiseProps() {
  var _this = this;

  this.gameMessage = function () {
    var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'SC_GM_QUERY';
    var GMRoot = _this.ProtoBuffer.GMRoot;

    return GMRoot.root.lookup(options.prefix + '.game.' + name);
  };

  this.makeData = function (buffer) {
    var zlibBuffer = compressData(buffer);
    var head = _pythonStruct2.default.pack('!i', zlibBuffer.length);
    var data = Buffer.concat([head, zlibBuffer]);
    return data;
  };

  this.decode = function (buffer, CMsg) {
    var CMsgBase = _this.ProtoBuffer.CMsgBase;

    var ungzipBuffer = decompressData(buffer);
    var message = CMsgBase.decode(ungzipBuffer);
    if (message.msgbody) {
      var msgbody = CMsg.decode(message.msgbody);
      message.msgbody = msgbody;
      return message;
    }
    return ungzipBuffer;
  };

  this.createPBBuffer = function (msgtype, csMsg, params) {
    var CMsgBase = _this.ProtoBuffer.CMsgBase;

    var msghead = _this.createHeadMessage(msgtype);
    var pbRealHead = CMsgBase.create({ msghead: msghead });
    var buffer_head = CMsgBase.encode(pbRealHead).finish();
    if (!csMsg) return buffer_head;

    var _getCsMsgInfo = getCsMsgInfo(csMsg),
        csMsgKey = _getCsMsgInfo.csMsgKey,
        socket_Root = _getCsMsgInfo.socket_Root;

    var CMsgBody = socket_Root.root.lookup(options.prefix + '.' + csMsg);
    var pbBody = _this.createBodyMessage(csMsg, params);
    var msgbody = CMsgBody.encode(pbBody).finish();
    var pbMessage = CMsgBase.create({ msghead: msghead, msgbody: msgbody });
    var buffer_message = CMsgBase.encode(pbMessage).finish();
    return buffer_message;
  };

  this.createHeadMessage = function (msgtype) {
    var CMsgHead = _this.ProtoBuffer.CMsgHead;

    return CMsgHead.create({
      msgtype: msgtype,
      msgcode: 1
    });
  };

  this.createBodyMessage = function (csMsg, params) {
    var _getCsMsgInfo2 = getCsMsgInfo(csMsg),
        csMsgKey = _getCsMsgInfo2.csMsgKey,
        socket_Root = _getCsMsgInfo2.socket_Root;

    var CMsgBody = socket_Root.root.lookup(options.prefix + '.' + csMsg);
    return CMsgBody.create(params);
  };
};

exports.default = protobuffer;


var getProtoBuffer = function getProtoBuffer(opts) {
  var dir = opts.path;
  var pbDir = _path2.default.resolve(process.cwd(), dir).replace(/([^ \/])$/, '$1/');
  var _ref = [],
      socketRoot = _ref[0],
      CMsgBase = _ref[1],
      CMsgHead = _ref[2],
      GMRoot = _ref[3];

  var socketPB = _path2.default.resolve(pbDir, opts.socket);
  if (_fsExtra2.default.existsSync(socketPB)) {
    socketRoot = _protobufjs2.default.loadSync(socketPB);
    CMsgBase = socketRoot.root.lookup(opts.prefix + '.socket.CMsgBase');
    CMsgHead = socketRoot.root.lookup(opts.prefix + '.socket.CMsgHead');
  }
  var gmPB = _path2.default.resolve(pbDir, opts.gmPB);
  if (_fsExtra2.default.existsSync(gmPB)) {
    GMRoot = _protobufjs2.default.loadSync(gmPB);
  }
  return { socketRoot: socketRoot, CMsgBase: CMsgBase, CMsgHead: CMsgHead, GMRoot: GMRoot };
};

var compressData = function compressData(buffer) {
  return _zlib2.default.gzipSync(buffer, {
    windowBits: 15,
    memLevel: 8
  });
};

var decompressData = function decompressData(buffer) {
  return _zlib2.default.unzipSync(buffer, {
    windowBits: 15,
    memLevel: 8
  });
};

var getCsMsgInfo = function getCsMsgInfo(csMsg) {
  var _csMsg$split = csMsg.split('.'),
      _csMsg$split2 = _slicedToArray(_csMsg$split, 1),
      csMsgKey = _csMsg$split2[0];

  var socket_Root = Root.get(csMsgKey) ? Root.get(csMsgKey) : socketRoot;
  return { csMsgKey: csMsgKey, socket_Root: socket_Root };
};

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("protobufjs");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("fs-extra");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("zlib");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("python-struct");

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _net = __webpack_require__(8);

var _net2 = _interopRequireDefault(_net);

var _ExBuffer = __webpack_require__(9);

var _ExBuffer2 = _interopRequireDefault(_ExBuffer);

var _url = __webpack_require__(10);

var _url2 = _interopRequireDefault(_url);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Href = null;

var Socket = function Socket(Uri) {
  _classCallCheck(this, Socket);

  _initialiseProps.call(this);

  var uri = /^((https|http|ftp|rtsp|mms)?:\/\/)/.test(Uri) ? Uri : 'rtsp://' + Uri;

  var _Url$parse = _url2.default.parse(uri),
      hostname = _Url$parse.hostname,
      port = _Url$parse.port;

  Href = { hostname: hostname, port: port };
};

var _initialiseProps = function _initialiseProps() {
  this.send = function (data) {
    var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var _Href = Href,
        hostname = _Href.hostname,
        port = _Href.port;

    var socket = new _net2.default.Socket();
    var exBuffer = new _ExBuffer2.default().uint32Head().bigEndian();
    return new Promise(function (resolve, reject) {
      // 连接 ...
      socket.connect(port, hostname, function () {
        try {
          socket.write(data);
        } catch (error) {
          reject(error);
        }
      });
      // 接收数据
      socket.on('data', function (data) {
        exBuffer.put(data);
      });
      // 拼接数据
      exBuffer.on('data', function (buffer) {
        if (buffer.length > 0) {
          try {
            resolve(buffer);
          } catch (error) {
            reject(error);
          }
          socket.destroy();
        }
      });
      // 错误处理
      socket.on('error', function (error) {
        reject(error);
      });
      // 关闭连接
      socket.on('close', function () {
        // console.log('close')
      });
    });
  };
};

exports.default = Socket;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("net");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("ExBuffer");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })
/******/ ]);
//# sourceMappingURL=main.map